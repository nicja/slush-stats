<?php

if (!array_key_exists('nicja_slush_api_key', $_COOKIE)) {
  $result = array(
    'error' => 1,
    'msg'   => 'Your API token is not set. Please refresh the page and enter your API token.',
  );
}

else {
  $apiKey = str_replace('"', '', $_COOKIE['nicja_slush_api_key']);
  $url    = "https://mining.bitcoin.cz/stats/json/$apiKey";
  //$url    = 'tmp.tmp';
  $data   = @json_decode(file_get_contents($url));
  
  if (!$data) {
    $result = array(
      'error' => 1,
      'msg'   => 'Error connecting to slush stats server. Maybe your API token is incorrect, or the server is down.',
    );
  }
  else {
    $maxtime = 0;
    $blocks  = array();
    $totals  = array(
      'rps'    => array(array('x', 'Reward per hour', 'Reward')),
      'dur'    => array(),
      'sps'    => array(array('x', 'Shares')),
      'rpsmax' => 0,
    );
    
    foreach ($data->blocks as $id => $block) {
      list($hours, $min, $sec) = explode(':', $block->mining_duration);
      $block->durationhr  = $hours + $min / 60 + $sec / 3600;
      $block->durationsec = $hours * 3600 + $min * 60 + $sec;
      $blocks[$id] = $block;
      $maxtime = ($maxtime > $block->durationhr)? $maxtime : $block->durationhr;
    }
    
    ksort($blocks);
    
    foreach ($blocks as $id => $block) {
      if (!property_exists($block, 'reward')) {
        $block->reward = 0;
      }
      $rps = $block->reward / $block->durationhr;
      //$duration = $block->durationhr / $maxtime * 0.002;
      $totals['rps'][] = array($id, round($rps, 8), round($block->reward, 8));
      $totals['dur'][] = array($id, $block->durationhr, $block->mining_duration);
      $totals['sps'][] = array($id, round($block->total_shares/$block->durationsec, 2));
      $totals['rpsmax'] = ($totals['rpsmax'] > $rps)? $totals['rpsmax'] : $rps;
      //echo number_format($totals['rps'][$id], 8). "<br />";
    }
    $result = array('error' => 0, 'data' => $totals);
  }
}

//var_dump($totals);
echo json_encode($result);
?>