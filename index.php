<?php
  $session = (array_key_exists('nicja_slush_api_key', $_COOKIE));
?>

<!doctype html>
<html ng-app="SlushStats">
  <head>
    <title>Slush Stats</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Simple visualisations of Bitcoin mining data from Slush's pool, mining.bitcoin.cz">
    <meta name="author" content="Nicja">
      
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Cache-control" content="no-store">
    
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="css/slush.css">
    
    <script src="js/angular.min.js"></script>
    <script src="js/angular-cookies.min.js"></script>
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      
      ga('create', 'UA-40902483-1', 'alwaysdata.net');
      ga('send', 'pageview');
      
    </script>
    
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="js/slush.js"></script>
    <script type="text/javascript"> 
      
      // Load the Visualization API and the piechart package.
      google.load('visualization', '1', {'packages':['corechart']});
      
      // Set a callback to run when the Google Visualization library is loaded.
      google.setOnLoadCallback(function() {
        angular.element(document.body).scope().$apply(function(scope) {
          scope.refreshStats();
        });
      });
      
    </script>
  </head>
  
  <body ng-controller="SlushCtrl">
    <div class="container-narrow">
      <div class="masthead">
        <ul class="nav nav-pills pull-right">
          <li class="active"><a href="#">Home</a></li>
          <?php if ($session): ?>
            <li><a href="?1" ng-click="destroyCookie()">Clear token</a></li>
          <?php endif; ?>
        </ul>
        <h3 class="muted">Slush Stats by nicja</h3>
      </div>
      
      <hr />
      
      <div class="error alert-error">{{error}}</div>
    
<?php if ($session): ?> 
  <div>
    <div ng-show="working" class="loading pull-right">Loading</div>
    <div ng-hide="working" class="small muted pull-right">
      Autorefresh in {{getRemaining()}}
      <a href='#' ng-show="counting" alt="Stop" ng-click="stopCountDown()">■</a>
      <a href='#' ng-hide="counting" alt="Play" ng-click="countDown()">►</a>
    </div>
    
    <p class="small muted">API Token: {{apiKey}}</p>
    <a href="#" class="btn btn-success" ng-click="refreshStats()">Refresh</a>
    <a href="?1" class="btn btn-warning" ng-click="destroyCookie()">Clear token</a>
    <br class="clear"/>
    <div id="graph-rps" class="graph"></div>
    <div id="graph-dur" class="graph"></div>
    <div id="graph-sps" class="graph"></div>
    
  </div>

<?php else: ?>
  <div>
    <label for="input-apikey">API Token</label>
    <input id="input-apikey" ng-model="apiKey" name="apikey" type="text" auto-complete />
    <a href="?1" class="btn btn-success" ng-click="saveApiKey()">Go</a>
  </div>

<?php endif; ?>

    </div>
  </body>
</html>
