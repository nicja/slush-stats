var app = angular.module('SlushStats', ['ngCookies']);

app.controller('SlushCtrl', function($scope, $http, $cookieStore, $timeout) {
  $scope.working   = false;
  $scope.counting  = true;
  $scope.apiKey    = $cookieStore.get('nicja_slush_api_key');
  
  var timerId;
  
  $scope.refreshStats = function() {
    if ($scope.apiKey) {
     try {
      $timeout.cancel(timerId);
      $scope.working = true;
      $scope.error   = '';
      $http({
        method: 'GET',
        url: 'php/slush.php',
        headers: {'Content-Type': 'application/json'}
      }).success(function(response, code) {
        if (response.error == 1) {
          $scope.error = response.msg;
        }
        else {
          var data = google.visualization.arrayToDataTable(response.data.rps);
          new google.visualization.LineChart(document.getElementById('graph-rps')).
            draw(data, {curveType: "line",
                        width: 800, height: 200,
                        title: 'Block Rewards'
                       }
          );
          
          var data2 = new google.visualization.DataTable();
          var dur = response.data.dur;
          data2.addColumn('number', 'Block');
          data2.addColumn('number', 'Duration');
          
          for (var i = 0; i < dur.length; i++) {
            data2.addRow([{v: dur[i][0], f: 'Block ' + dur[i][0]}, {v: dur[i][1], f: dur[i][2]}]);
          }
          
          new google.visualization.LineChart(document.getElementById('graph-dur')).
            draw(data2, {curveType: "line",
                        width: 800, height: 200,
                        title: 'Block Duration'
                       }
          );
          
          var data3 = google.visualization.arrayToDataTable(response.data.sps);
          new google.visualization.LineChart(document.getElementById('graph-sps')).
            draw(data3, {curveType: "line",
                        width: 800, height: 200,
                        title: 'Network shares per second'
                       }
          );
        }
        $scope.remaining = 600;
        $scope.working = false;
        $scope.countDown();
        
      }).error(function(response, code) {
        $scope.error = "AJAX Error";
        $scope.response = response.data;
        $scope.code = response.code;
        $scope.working = false;
        return false;
      });
    }
    catch (ex) {
      $scope.error = "Uncaught exception: ex";
    }
   }
  }
  
  $scope.countDown = function() {
    $scope.counting = true;
    if ($scope.remaining == 0) {
      $scope.refreshStats();
    }
    else {
      timerId = $timeout(function() {
        $scope.remaining--;
        $scope.countDown();
      }, 1000);
    }
  }
  
  $scope.stopCountDown = function() {
    $timeout.cancel(timerId);
    $scope.counting = false;
    return false;
  }
  
  $scope.getRemaining = function() {
    var m, s;
    m = ('0' + Math.floor($scope.remaining / 60)).slice(-2);
    s = ('0' + Math.floor($scope.remaining - 60 * m)).slice(-2);
    return m + ':' + s;
  }
  
  $scope.saveApiKey = function() {
    $cookieStore.put('nicja_slush_api_key', $scope.apiKey);
    window.location.reload(true);
  }
  
  $scope.destroyCookie = function() {
    $cookieStore.remove('nicja_slush_api_key');
    window.location.reload(true);
  }
});